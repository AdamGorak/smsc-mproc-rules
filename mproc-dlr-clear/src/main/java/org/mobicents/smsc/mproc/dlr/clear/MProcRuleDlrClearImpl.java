/*
 * Telestax, Open Source Cloud Communications Copyright 2011-2017,
 * Telestax Inc and individual contributors by the @authors tag.
 * See the copyright.txt in the distribution for a full listing of
 * individual contributors.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */

package org.mobicents.smsc.mproc.dlr.clear;

import org.apache.log4j.Logger;
import org.mobicents.smsc.mproc.MProcMessage;
import org.mobicents.smsc.mproc.MProcRuleBaseImpl;
import org.mobicents.smsc.mproc.MProcRuleRaProvider;
import org.mobicents.smsc.mproc.PostArrivalProcessor;

import javolution.xml.XMLFormat;
import javolution.xml.stream.XMLStreamException;

/**
 * The Class MProcRuleDlrClearImpl.
 */
public final class MProcRuleDlrClearImpl extends MProcRuleBaseImpl {

    private static final Logger LOG = Logger.getLogger(MProcRuleDlrClearImpl.class);

    private static final String SEPARATOR = "=";
    private static final String NETWORK_ID_MASK = "networkidmask";
    private static final String NETWORK_ID_MASK_XML = "networkIdMask";

    private static final int NETWORK_ID_MASK_UNSET = -1;

    private int itsNetworkIdMask = NETWORK_ID_MASK_UNSET;

    @Override
    public String getRuleClassName() {
        return MProcRuleDlrClearFactoryImpl.CLASS_NAME;
    }

    /**
     * Gets the network ID mask.
     *
     * @return the network ID mask
     */
    public int getNetworkIdMask() {
        return itsNetworkIdMask;
    }

    /**
     * Sets the network ID mask.
     *
     * @param aNetworkIdMask the network ID mask
     */
    public void setNetworkIdMask(final int aNetworkIdMask) {
        itsNetworkIdMask = aNetworkIdMask;
        LOG.info("NetworkIdMask changed to: " + itsNetworkIdMask + ".");
    }

    @Override
    public void setInitialRuleParameters(final String aParametersString) throws MProcRuleDlrClearException {
        setRuleParameters(aParametersString);
    }

    @Override
    public void updateRuleParameters(final String aParametersString) throws Exception {
        setRuleParameters(aParametersString);
    }

    private void setRuleParameters(final String aParametersString) throws MProcRuleDlrClearException {
        if (aParametersString == null) {
            setNetworkIdMask(NETWORK_ID_MASK_UNSET);
            return;
        }
        if (aParametersString.isEmpty()) {
            setNetworkIdMask(NETWORK_ID_MASK_UNSET);
            return;
        }
        final String[] nv = aParametersString.split(SEPARATOR);
        if (nv.length != 2) {
            throw new MProcRuleDlrClearException("Invalid rule parameter value: " + aParametersString + ".");
        }
        if (!nv[0].equals(NETWORK_ID_MASK)) {
            throw new MProcRuleDlrClearException("Invalid rule parameter value: " + nv[0] + ".");
        }
        try {
            setNetworkIdMask(Integer.parseInt(nv[1]));
        } catch (NumberFormatException e) {
            throw new MProcRuleDlrClearException("Invalid rule parameter value: " + nv[1] + ".");
        }

    }

    @Override
    public String getRuleParameters() {
        final StringBuilder sb = new StringBuilder();
        if (itsNetworkIdMask != -1) {
            writeParameter(sb, 0, NETWORK_ID_MASK, itsNetworkIdMask, null, SEPARATOR);
        }
        return sb.toString();
    }

    @Override
    public boolean isForPostArrivalState() {
        return true;
    }

    @Override
    public boolean matchesPostArrival(final MProcMessage aMessage) {
        if (itsNetworkIdMask < 0) {
            return false;
        }
        return itsNetworkIdMask == aMessage.getNetworkId();
    }

    @Override
    public void onPostArrival(final MProcRuleRaProvider anMProcRuleRa, final PostArrivalProcessor aProcessor,
            final MProcMessage aMessage) {
        final int rd = aMessage.getRegisteredDelivery();
        aProcessor.updateRegisteredDelivery_DeliveryReceiptNo(aMessage);
        if (LOG.isDebugEnabled()) {
            LOG.debug("DeliveryReceipt cleared in message : " + aMessage.getSourceAddr() + " => "
                    + aMessage.getDestAddr() + ". Registered Delivery was: " + rd + ".");
        }
    }

    /**
     * XML Serialization/Deserialization.
     */
    protected static final XMLFormat<MProcRuleDlrClearImpl> M_PROC_RULE_TEST_XML = new XMLFormat<MProcRuleDlrClearImpl>(
            MProcRuleDlrClearImpl.class) {

        @Override
        public void read(final XMLFormat.InputElement xml, final MProcRuleDlrClearImpl mProcRule)
                throws XMLStreamException {
            M_PROC_RULE_BASE_XML.read(xml, mProcRule);
            mProcRule.setNetworkIdMask(xml.getAttribute(NETWORK_ID_MASK_XML, -1));
        }

        @Override
        public void write(final MProcRuleDlrClearImpl mProcRule, final XMLFormat.OutputElement xml)
                throws XMLStreamException {
            M_PROC_RULE_BASE_XML.write(mProcRule, xml);
            final int nwIdMask = mProcRule.getNetworkIdMask();
            if (nwIdMask != -1) {
                xml.setAttribute(NETWORK_ID_MASK_XML, nwIdMask);
            }
        }
    };

}
