/*
 * Telestax, Open Source Cloud Communications Copyright 2011-2017,
 * Telestax Inc and individual contributors by the @authors tag.
 * See the copyright.txt in the distribution for a full listing of
 * individual contributors.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */

package org.mobicents.smsc.mproc.retry;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.mobicents.smsc.mproc.MProcMessage;
import org.mobicents.smsc.mproc.MProcRuleBaseImpl;
import org.mobicents.smsc.mproc.MProcRuleException;
import org.mobicents.smsc.mproc.MProcRuleRaProvider;
import org.mobicents.smsc.mproc.PostDeliveryProcessor;

import javolution.xml.XMLFormat;
import javolution.xml.stream.XMLStreamException;

/**
 * The Class MProcRuleDlrClearImpl.
 */
public final class MProcRuleRetryImpl extends MProcRuleBaseImpl {

    private static final Logger LOG = Logger.getLogger(MProcRuleRetryImpl.class);

    private static final String SEPARATOR = "=";
    private static final String SEPARATOR_PARAMETERS = " ";
    private static final String NETWORK_ID_MASK = "networkidmask";
    private static final String NETWORK_ID_MASK_XML = "networkIdMask";
    private static final String SMPP_COMMAND_STATUS_MASK = "smppcommandstatusmask";
    private static final String SMPP_COMMAND_STATUS_MASK_XML = "smppCommandStatusMask";

    private static final int UNSET = -1;

    private static final int CLEANUP_INTERVAL_MILIS = 1000;
    private static final int TIME_TO_LIVE_MILIS = 5000;

    private int itsNetworkIdMask = UNSET;
    private int itsSmppCommandStatusMask = UNSET;

    private long itsLastCleanupTimestamp;
    private final Map<String, Long> itsRetriedDestinations;

    public MProcRuleRetryImpl() {
        itsLastCleanupTimestamp = System.currentTimeMillis();
        itsRetriedDestinations = Collections.synchronizedMap(new HashMap<String, Long>());
    }

    @Override
    public String getRuleClassName() {
        return MProcRuleRetryFactoryImpl.CLASS_NAME;
    }

    @Override
    public void setInitialRuleParameters(final String aParametersString) throws MProcRuleRetryException {
        setRuleParameters(aParametersString);
    }

    @Override
    public void updateRuleParameters(final String aParametersString) throws Exception {
        setRuleParameters(aParametersString);
    }

    @Override
    public String getRuleParameters() {
        final StringBuilder sb = new StringBuilder();
        sb.append(NETWORK_ID_MASK);
        if (itsNetworkIdMask > -1) {
            sb.append(SEPARATOR);
            sb.append(itsNetworkIdMask);
        }
        sb.append(SEPARATOR_PARAMETERS);
        sb.append(SMPP_COMMAND_STATUS_MASK);
        if (itsSmppCommandStatusMask > -1) {
            sb.append(SEPARATOR);
            sb.append(itsSmppCommandStatusMask);
        }
        return sb.toString();
    }

    @Override
    public boolean isForPostDeliveryState() {
        return true;
    }

    @Override
    public boolean matchesPostDelivery(final MProcMessage aMessage) {
        if (itsNetworkIdMask < 0) {
            return false;
        }
        if (itsSmppCommandStatusMask < 0) {
            return false;
        }
        return (itsNetworkIdMask == aMessage.getNetworkId())
                && (itsSmppCommandStatusMask == aMessage.getSmppCommandStatus());
    }

    @Override
    public synchronized void onPostDelivery(final MProcRuleRaProvider anMProcRuleRa,
            final PostDeliveryProcessor aProcessor, final MProcMessage aMessage) throws MProcRuleException {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Retrying message of : " + aMessage.getSourceAddr() + " => " + aMessage.getDestAddr() + ".");
        }
        final String d = aMessage.getDestAddr();
        if (itsRetriedDestinations.containsKey(d)) {
            LOG.info("Skipping retrying message of : " + aMessage.getSourceAddr() + " => " + aMessage.getDestAddr()
                    + " (destination recently retried)");
            return;
        }
        aProcessor.rerouteMessage(itsNetworkIdMask);
        putAndCleanup(d);
    }

    private void putAndCleanup(final String aDestination) {
        final long ct = System.currentTimeMillis();
        itsRetriedDestinations.put(aDestination, ct + TIME_TO_LIVE_MILIS);
        if (ct - itsLastCleanupTimestamp > CLEANUP_INTERVAL_MILIS) {
            for (final String k : itsRetriedDestinations.keySet()) {
                if (itsRetriedDestinations.get(k).longValue() < ct) {
                    itsRetriedDestinations.remove(k);
                }
            }
        }
    }

    /**
     * Gets the network ID mask.
     *
     * @return the network ID mask
     */
    public int getNetworkIdMask() {
        return itsNetworkIdMask;
    }

    /**
     * Sets the network ID mask.
     *
     * @param aNetworkIdMask the network ID mask
     */
    public void setNetworkIdMask(final int aNetworkIdMask) {
        itsNetworkIdMask = aNetworkIdMask;
        LOG.info("NetworkIdMask changed to: " + itsNetworkIdMask + ".");
    }

    /**
     * Gets the SMPP command status mask.
     *
     * @return the SMPP command status mask
     */
    public int getSmppCommandStatusMask() {
        return itsSmppCommandStatusMask;
    }

    /**
     * Sets the SMPP command status mask.
     *
     * @param anSmppCommandStatusMask the new SMPP command status mask
     */
    public void setSmppCommandStatusMask(final int anSmppCommandStatusMask) {
        itsSmppCommandStatusMask = anSmppCommandStatusMask;
    }

    private void setRuleParameters(final String aParametersString) throws MProcRuleRetryException {
        if (aParametersString == null) {
            setNetworkIdMask(UNSET);
            setSmppCommandStatusMask(UNSET);
            return;
        }
        if (aParametersString.isEmpty()) {
            setNetworkIdMask(UNSET);
            setSmppCommandStatusMask(UNSET);
            return;
        }
        final String[] parameters = aParametersString.split(SEPARATOR_PARAMETERS);
        if (parameters.length < 2) {
            throw new MProcRuleRetryException("Invalid rule parameters value: " + aParametersString + ".");
        }
        for (final String ps : parameters) {
            final String[] p = ps.split(SEPARATOR);
            if (p.length < 1) {
                throw new MProcRuleRetryException("Invalid value of parameter: " + ps + ".");
            }
            if (p.length < 2) {
                if (NETWORK_ID_MASK.equals(p[0])) {
                    setNetworkIdMask(UNSET);
                } else if (SMPP_COMMAND_STATUS_MASK.equals(p[0])) {
                    setSmppCommandStatusMask(UNSET);
                }
            } else {
                if (NETWORK_ID_MASK.equals(p[0])) {
                    try {
                        setNetworkIdMask(Integer.parseInt(p[1]));
                    } catch (NumberFormatException e) {
                        throw new MProcRuleRetryException("Invalid value of NetworkIdMask: " + p[1] + ".");
                    }
                } else if (SMPP_COMMAND_STATUS_MASK.equals(p[0])) {
                    try {
                        setSmppCommandStatusMask(Integer.parseInt(p[1]));
                    } catch (NumberFormatException e) {
                        throw new MProcRuleRetryException("Invalid value of SmppCommandStatusMask: " + p[1] + ".");
                    }
                }
            }
        }
    }

    /**
     * XML Serialization/Deserialization.
     */
    protected static final XMLFormat<MProcRuleRetryImpl> M_PROC_RULE_TEST_XML = new XMLFormat<MProcRuleRetryImpl>(
            MProcRuleRetryImpl.class) {

        @Override
        public void read(final XMLFormat.InputElement xml, final MProcRuleRetryImpl mProcRule)
                throws XMLStreamException {
            M_PROC_RULE_BASE_XML.read(xml, mProcRule);
            mProcRule.setNetworkIdMask(xml.getAttribute(NETWORK_ID_MASK_XML, -1));
            mProcRule.setSmppCommandStatusMask(xml.getAttribute(SMPP_COMMAND_STATUS_MASK_XML, -1));
        }

        @Override
        public void write(final MProcRuleRetryImpl mProcRule, final XMLFormat.OutputElement xml)
                throws XMLStreamException {
            M_PROC_RULE_BASE_XML.write(mProcRule, xml);
            final int nwIdMask = mProcRule.getNetworkIdMask();
            if (nwIdMask != -1) {
                xml.setAttribute(NETWORK_ID_MASK_XML, nwIdMask);
            }
            final int smppCommandStatusMask = mProcRule.getSmppCommandStatusMask();
            if (smppCommandStatusMask != -1) {
                xml.setAttribute(SMPP_COMMAND_STATUS_MASK_XML, smppCommandStatusMask);
            }
        }
    };

}
