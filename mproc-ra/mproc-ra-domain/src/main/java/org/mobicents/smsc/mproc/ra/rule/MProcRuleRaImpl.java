/*
 * Telestax, Open Source Cloud Communications Copyright 2011-2017,
 * Telestax Inc and individual contributors by the @authors tag.
 * See the copyright.txt in the distribution for a full listing of
 * individual contributors.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package org.mobicents.smsc.mproc.ra.rule;

import org.apache.log4j.Logger;
import org.mobicents.smsc.mproc.MProcMessage;
import org.mobicents.smsc.mproc.MProcRuleBaseImpl;
import org.mobicents.smsc.mproc.MProcRuleRaException;
import org.mobicents.smsc.mproc.MProcRuleRaProvider;
import org.mobicents.smsc.mproc.PostArrivalProcessor;
import org.mobicents.smsc.mproc.PostDeliveryTempFailureProcessor;

import javolution.xml.XMLFormat;
import javolution.xml.stream.XMLStreamException;

/**
 * The Class MProcRuleRaImpl.
 */
public final class MProcRuleRaImpl extends MProcRuleBaseImpl {

    private static final Logger LOG = Logger.getLogger(MProcRuleRaImpl.class);

    private static final String EMPTY = "";
    private static final String PARAMETER_NETWORK_ID = "networkIdMask";

    private static final String POST_ARRIVAL = "PostArrival";
    private static final String POST_DELIVERY_TEMP_FAILURE = "PostDeliveryTempFailure";

    private String itsNetworkId;
    private Integer itsNetworkIdValue;

    /**
     * Instantiates a new MProc rule RA implementation.
     */
    public MProcRuleRaImpl() {
        super();
    }

    @Override
    public void setInitialRuleParameters(final String aParametersString) {
        setNetworkIds(aParametersString);
    }

    @Override
    public void updateRuleParameters(final String aParametersString) {
        setNetworkIds(aParametersString);
    }

    @Override
    public String getRuleClassName() {
        return MProcRuleRaFactoryImpl.RULE_CLASS_NAME;
    }

    @Override
    public String getRuleParameters() {
        return itsNetworkId;
    }

    @Override
    public boolean isForPostArrivalState() {
        return true;
    }

    @Override
    public boolean isForPostDeliveryTempFailureState() {
        return true;
    }

    @Override
    public boolean matchesPostArrival(final MProcMessage aMessage) {
        return matches(aMessage, POST_ARRIVAL);
    }

    @Override
    public boolean matchesPostDeliveryTempFailure(final MProcMessage aMessage) {
        return matches(aMessage, POST_DELIVERY_TEMP_FAILURE);
    }

    @Override
    public void onPostArrival(final MProcRuleRaProvider anMProcRuleRa, final PostArrivalProcessor aProcessor,
            final MProcMessage aMessage) throws MProcRuleRaException {
        anMProcRuleRa.onPostArrival(aProcessor, aMessage);
    }

    @Override
    public void onPostDeliveryTempFailure(final MProcRuleRaProvider anMProcRuleRa,
            final PostDeliveryTempFailureProcessor aProcessor, final MProcMessage aMessage)
            throws MProcRuleRaException {
        anMProcRuleRa.onPostDeliveryTempFailure(aProcessor, aMessage);
    }

    private void setNetworkIds(final String aParameterValue) {
        if (aParameterValue == null) {
            itsNetworkId = EMPTY;
            itsNetworkIdValue = null;
            return;
        }
        try {
            itsNetworkIdValue = Integer.parseInt(aParameterValue);
            itsNetworkId = aParameterValue;
        } catch (NumberFormatException e) {
            LOG.warn("Unable to parse value '" + aParameterValue + "' as integer.", e);
            itsNetworkId = EMPTY;
            itsNetworkIdValue = null;
        }
    }

    private boolean matches(final MProcMessage aMessage, final String aMethod) {
        if (itsNetworkIdValue == null) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("Rule: #" + getId() + ". " + aMethod + ". NetworkId is not set - accepting.");
            }
            return true;
        }
        if (itsNetworkIdValue.intValue() < 0) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("Rule: #" + getId() + ". " + aMethod + ". NetworkId is below 0 - accepting.");
            }
            return true;
        }
        if (LOG.isDebugEnabled()) {
            LOG.debug("Rule: #" + getId() + ". " + aMethod + ". NetworkId: " + itsNetworkIdValue
                    + ". Message NetworkId: " + aMessage.getNetworkId() + ".");
        }
        return itsNetworkIdValue.intValue() == aMessage.getNetworkId();
    }

    protected static final XMLFormat<MProcRuleRaImpl> M_PROC_RULE_RA_IMPL_XML = new XMLFormat<MProcRuleRaImpl>(
            MProcRuleRaImpl.class) {

        public void read(final InputElement anXml, final MProcRuleRaImpl aRule) throws XMLStreamException {
            M_PROC_RULE_BASE_XML.read(anXml, aRule);
            aRule.setInitialRuleParameters(anXml.getAttribute(PARAMETER_NETWORK_ID, EMPTY));
        }

        public void write(final MProcRuleRaImpl aRule, final OutputElement anXml) throws XMLStreamException {
            M_PROC_RULE_BASE_XML.write(aRule, anXml);
            final String nids = aRule.itsNetworkId;
            if (nids == null) {
                return;
            }
            if (nids.isEmpty()) {
                return;
            }
            anXml.setAttribute(PARAMETER_NETWORK_ID, nids);
        }

    };

}
