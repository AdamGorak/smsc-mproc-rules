/*
 * Telestax, Open Source Cloud Communications Copyright 2011-2017,
 * Telestax Inc and individual contributors by the @authors tag.
 * See the copyright.txt in the distribution for a full listing of
 * individual contributors.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package org.mobicents.smsc.mproc.ra.test;

import javax.slee.resource.ResourceAdaptor;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.junit.runner.RunWith;
import org.mobicents.smsc.mproc.MProcRuleRaException;
import org.mobicents.smsc.mproc.MProcRuleRaProvider;
import org.mobicents.smsc.mproc.ra.test.stub.MProcMessageStub;
import org.mobicents.smsc.mproc.ra.test.stub.PostArrivalProcessorStub;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * The Class CustomMprocRaTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/custom-mproc-ra-test-context.xml" })
public class CustomMprocRaTest extends AbstractJUnit4SpringContextTests {

    private static final Log LOG = LogFactory.getLog(CustomMprocRaTest.class);

    /**
     * Test database connection.
     *
     * @throws MProcRuleRaException the MProc Rule RA exception
     */
    @Test
    public void testPostArrival() throws MProcRuleRaException {
        final MProcRuleRaProvider ra = applicationContext.getBean(MProcRuleRaProvider.class);
        ra.onPostArrival(new PostArrivalProcessorStub(), new MProcMessageStub());
    }

    /**
     * Test RA entity restart.
     */
    @Test
    public void testRaEntityRestart() {
        final ResourceAdaptor ra = applicationContext.getBean(ResourceAdaptor.class);
        ra.raStopping();
        ra.raInactive();
        ra.raActive();
    }

    /**
     * Used to get current executing test name.
     */
    @Rule
    public final TestName name = new TestName();

    /**
     * Tests WATCHMAN.
     */
    @Rule
    public final TestWatcher WATCHMAN = new TestWatcher() {
        protected void starting(final Description aDescription) {
            if (LOG.isInfoEnabled()) {
                LOG.info("TEST START: " + aDescription.getMethodName() + ".");
            }
        }

        protected void succeeded(final Description aDescription) {
            if (LOG.isInfoEnabled()) {
                LOG.info("TEST PASS : " + aDescription.getMethodName() + ".");
            }
        }

        protected void failed(final Throwable e, final Description aDescription) {
            if (LOG.isWarnEnabled()) {
                LOG.warn("TEST FAIL : " + aDescription.getMethodName() + " (" + e.getMessage() + ").", e);
            }
        }
    };
}
