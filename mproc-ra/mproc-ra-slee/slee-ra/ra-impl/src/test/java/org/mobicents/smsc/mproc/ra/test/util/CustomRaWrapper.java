/*
 * Telestax, Open Source Cloud Communications Copyright 2011-2017,
 * Telestax Inc and individual contributors by the @authors tag.
 * See the copyright.txt in the distribution for a full listing of
 * individual contributors.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package org.mobicents.smsc.mproc.ra.test.util;

import javax.slee.resource.ConfigProperties;
import javax.slee.resource.InvalidConfigurationException;
import javax.slee.resource.ResourceAdaptor;
import javax.slee.resource.ResourceAdaptorContext;

import org.springframework.beans.InvalidPropertyException;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

/**
 * The Class CustomRaWrapper.
 */
public final class CustomRaWrapper implements InitializingBean, DisposableBean {

    private ResourceAdaptor itsResourceAdaptor;
    private ResourceAdaptorContext itsResourceAdaptorContext;
    private CustomRaConfigurationProvider itsConfigurationProvider;

    @Override
    public void afterPropertiesSet() throws InvalidConfigurationException {
        if (itsResourceAdaptor == null) {
            throw new InvalidPropertyException(CustomRaWrapper.class, "ResourceAdaptor", "Not set.");
        }
        if (itsResourceAdaptorContext == null) {
            throw new InvalidPropertyException(CustomRaWrapper.class, "ResourceAdaptorContext", "Not set.");
        }
        if (itsConfigurationProvider == null) {
            throw new InvalidPropertyException(CustomRaWrapper.class, "ConfigurationProvider", "Not set.");
        }
        itsResourceAdaptor.setResourceAdaptorContext(itsResourceAdaptorContext);
        final ConfigProperties cp = itsConfigurationProvider.getConfiguration();
        itsResourceAdaptor.raVerifyConfiguration(cp);
        itsResourceAdaptor.raConfigure(cp);
        itsResourceAdaptor.raActive();
    }

    @Override
    public void destroy() {
        itsResourceAdaptor.raStopping();
        itsResourceAdaptor.raInactive();
    }

    /**
     * Sets the resource adaptor.
     *
     * @param aResourceAdaptor the new resource adaptor
     */
    public void setResourceAdaptor(final ResourceAdaptor aResourceAdaptor) {
        itsResourceAdaptor = aResourceAdaptor;
    }

    /**
     * Sets the configuration provider.
     *
     * @param aConfigurationProvider the new configuration provider
     */
    public void setConfigurationProvider(final CustomRaConfigurationProvider aConfigurationProvider) {
        itsConfigurationProvider = aConfigurationProvider;
    }

    /**
     * Sets the resource adaptor context.
     *
     * @param aResourceAdaptorContext the new resource adaptor context
     */
    public void setResourceAdaptorContext(final ResourceAdaptorContext aResourceAdaptorContext) {
        itsResourceAdaptorContext = aResourceAdaptorContext;
    }

}
