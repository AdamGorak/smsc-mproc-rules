/*
 * Telestax, Open Source Cloud Communications Copyright 2011-2017,
 * Telestax Inc and individual contributors by the @authors tag.
 * See the copyright.txt in the distribution for a full listing of
 * individual contributors.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package org.mobicents.smsc.mproc.ra.test.stub;

import org.mobicents.smsc.mproc.ra.CustomMProcRuleResourceAdaptorUsageParameters;

/**
 * The Class CustomMProcRuleResourceAdaptorUsageParametersStub.
 */
public final class CustomMProcRuleResourceAdaptorUsageParametersStub
        implements CustomMProcRuleResourceAdaptorUsageParameters {

    @Override
    public void incrementTotalInvocations(final long aValue) {
    }

    @Override
    public void incrementRedirections(final long aValue) {
    }

    @Override
    public void incrementContinuations(final long aValue) {
    }

    @Override
    public void incrementUnsuccessfulRedirections(final long aValue) {
    }

    @Override
    public void incrementPostArrivalInvocations(final long aValue) {
    }

    @Override
    public void samplePostArrivalResponseTime(final long aValue) {
    }

    @Override
    public void incrementPostDeliveryTempFailureInvocations(final long aValue) {
    }

    @Override
    public void samplePostDeliveryTempFailureResponseTime(final long aValue) {
    }

    @Override
    public void incrementRejections(final long aValue) {
    }

    @Override
    public void incrementInternalErrors(final long aValue) {
    }

    @Override
    public void incrementMessageAlreadyRejectedOrDropped(final long aValue) {
    }

}
