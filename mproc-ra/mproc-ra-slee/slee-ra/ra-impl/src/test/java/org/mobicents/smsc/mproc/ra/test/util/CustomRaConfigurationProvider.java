/*
 * Telestax, Open Source Cloud Communications Copyright 2011-2017,
 * Telestax Inc and individual contributors by the @authors tag.
 * See the copyright.txt in the distribution for a full listing of
 * individual contributors.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package org.mobicents.smsc.mproc.ra.test.util;

import javax.slee.resource.ConfigProperties;

import org.springframework.beans.factory.InitializingBean;

/**
 * The Class CustomRaConfigurationProvider.
 */
public final class CustomRaConfigurationProvider implements InitializingBean {

    private static final String P_NODE_NAME = "Node.Name";
    private static final String P_CONTEXT_FILE_NAME = "Context.File.Name";
    private static final String P_CONTEXT_PACKAGE_SCAN_BASE = "Context.Package.Scan.Base";

    private static final String P_DB_DATA_SOURCE_JNDI = "Db.Internal.DataSource.Jndi";
    private static final String P_DB_URL = "Db.Internal.Url";
    private static final String P_DB_USER = "Db.Internal.User";
    private static final String P_DB_PASS = "Db.Internal.Password";
    private static final String P_DB_DRIVER = "Db.Internal.DriverClassName";
    private static final String P_DB_VALIDATION_QUERY = "Db.Internal.ValidationQuery";
    private static final String P_DB_CONNECTION_PROPERTIES = "Db.Internal.ConnectionProperties";

    private String itsNodeName;
    private String itsContextFileName;
    private String itsContextPackageScanBase;

    private String itsDbDataSourceJndi;
    private String itsDbUrl;
    private String itsDbUser;
    private String itsDbPassword;
    private String itsDbDriverClassName;
    private String itsDbValidationQuery;
    private String itsDbConnectionProperties;

    private ConfigProperties itsConfigProperties;

    @Override
    public void afterPropertiesSet() {
        itsConfigProperties = new ConfigProperties();
        itsConfigProperties
                .addProperty(new ConfigProperties.Property(P_NODE_NAME, String.class.getCanonicalName(), itsNodeName));
        itsConfigProperties.addProperty(new ConfigProperties.Property(P_CONTEXT_FILE_NAME,
                String.class.getCanonicalName(), itsContextFileName));
        itsConfigProperties.addProperty(new ConfigProperties.Property(P_CONTEXT_PACKAGE_SCAN_BASE,
                String.class.getCanonicalName(), itsContextPackageScanBase));
        itsConfigProperties.addProperty(new ConfigProperties.Property(P_DB_DATA_SOURCE_JNDI,
                String.class.getCanonicalName(), itsDbDataSourceJndi));
        itsConfigProperties
                .addProperty(new ConfigProperties.Property(P_DB_URL, String.class.getCanonicalName(), itsDbUrl));
        itsConfigProperties
                .addProperty(new ConfigProperties.Property(P_DB_USER, String.class.getCanonicalName(), itsDbUser));
        itsConfigProperties
                .addProperty(new ConfigProperties.Property(P_DB_PASS, String.class.getCanonicalName(), itsDbPassword));
        itsConfigProperties.addProperty(
                new ConfigProperties.Property(P_DB_DRIVER, String.class.getCanonicalName(), itsDbDriverClassName));
        itsConfigProperties.addProperty(new ConfigProperties.Property(P_DB_VALIDATION_QUERY,
                String.class.getCanonicalName(), itsDbValidationQuery));
        itsConfigProperties.addProperty(new ConfigProperties.Property(P_DB_CONNECTION_PROPERTIES,
                String.class.getCanonicalName(), itsDbConnectionProperties));
    }

    /**
     * Gets the configuration.
     *
     * @return the configuration
     */
    public ConfigProperties getConfiguration() {
        return itsConfigProperties;
    }

    /**
     * Sets the context file name.
     *
     * @param contextFileName the new context file name
     */
    public void setContextFileName(final String contextFileName) {
        itsContextFileName = contextFileName;
    }

    /**
     * Sets the context package scan base.
     *
     * @param aContextPackageScanBase the new context package scan base
     */
    public void setContextPackageScanBase(final String aContextPackageScanBase) {
        itsContextPackageScanBase = aContextPackageScanBase;
    }

    /**
     * Sets the node name.
     *
     * @param aNodeName the new node name
     */
    public void setNodeName(final String aNodeName) {
        itsNodeName = aNodeName;
    }

    /**
     * Sets the database data source JNDI.
     *
     * @param aDbDataSourceJndi the new database data source jndi
     */
    public void setDbDataSourceJndi(final String aDbDataSourceJndi) {
        itsDbDataSourceJndi = aDbDataSourceJndi;
    }

    /**
     * Sets the database url.
     *
     * @param aDbUrl the new database url
     */
    public void setDbUrl(final String aDbUrl) {
        itsDbUrl = aDbUrl;
    }

    /**
     * Sets the database user.
     *
     * @param aDbUser the new database user
     */
    public void setDbUser(final String aDbUser) {
        itsDbUser = aDbUser;
    }

    /**
     * Sets the database password.
     *
     * @param aDbPassword the new database password
     */
    public void setDbPassword(final String aDbPassword) {
        itsDbPassword = aDbPassword;
    }

    /**
     * Sets the database driver class name.
     *
     * @param aDbDriverClassName the new database driver class name
     */
    public void setDbDriverClassName(final String aDbDriverClassName) {
        itsDbDriverClassName = aDbDriverClassName;
    }

    /**
     * Sets the database validation query.
     *
     * @param aDbValidationQuery the new database validation query
     */
    public void setDbValidationQuery(final String aDbValidationQuery) {
        itsDbValidationQuery = aDbValidationQuery;
    }

    /**
     * Sets the database connection properties.
     *
     * @param aDbConnectionProperties the new database connection properties
     */
    public void setDbConnectionProperties(final String aDbConnectionProperties) {
        itsDbConnectionProperties = aDbConnectionProperties;
    }

    /**
     * Sets the config properties.
     *
     * @param configProperties the new config properties
     */
    public void setConfigProperties(final ConfigProperties configProperties) {
        itsConfigProperties = configProperties;
    }

}
