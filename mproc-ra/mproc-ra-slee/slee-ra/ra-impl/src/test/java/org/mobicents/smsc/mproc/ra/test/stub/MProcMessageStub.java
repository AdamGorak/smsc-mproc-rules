/*
 * Telestax, Open Source Cloud Communications Copyright 2011-2017,
 * Telestax Inc and individual contributors by the @authors tag.
 * See the copyright.txt in the distribution for a full listing of
 * individual contributors.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package org.mobicents.smsc.mproc.ra.test.stub;

import java.util.Date;

import org.mobicents.smsc.mproc.DeliveryReceiptData;
import org.mobicents.smsc.mproc.MProcMessage;
import org.mobicents.smsc.mproc.OrigType;
import org.mobicents.smsc.mproc.ProcessingType;
import org.restcomm.smpp.parameter.TlvSet;

/**
 * The Class MProcMessageStub.
 */
public final class MProcMessageStub implements MProcMessage {

    private static final int NETWORK_ID = 200;
    private static final int ORIG_NETWORK_ID = 100;

    @Override
    public long getMessageId() {
        return 0L;
    }

    @Override
    public int getSourceAddrTon() {
        return 0;
    }

    @Override
    public int getSourceAddrNpi() {
        return 0;
    }

    @Override
    public String getSourceAddr() {
        return "48100200300";
    }

    @Override
    public int getDestAddrTon() {
        return 0;
    }

    @Override
    public int getDestAddrNpi() {
        return 0;
    }

    @Override
    public String getDestAddr() {
        return "96800700600";
    }

    @Override
    public String getShortMessageText() {
        return "Short Message";
    }

    @Override
    public byte[] getShortMessageBin() {
        return null;
    }

    @Override
    public int getNetworkId() {
        return NETWORK_ID;
    }

    @Override
    public int getOrigNetworkId() {
        return ORIG_NETWORK_ID;
    }

    @Override
    public String getOrigEsmeName() {
        return null;
    }

    @Override
    public OrigType getOriginationType() {
        return null;
    }

    @Override
    public Date getScheduleDeliveryTime() {
        return null;
    }

    @Override
    public Date getValidityPeriod() {
        return null;
    }

    @Override
    public int getDataCoding() {
        return 0;
    }

    @Override
    public int getNationalLanguageSingleShift() {
        return 0;
    }

    @Override
    public int getNationalLanguageLockingShift() {
        return 0;
    }

    @Override
    public int getEsmClass() {
        return 0;
    }

    @Override
    public int getPriority() {
        return 0;
    }

    @Override
    public int getRegisteredDelivery() {
        return 0;
    }

    @Override
    public String getOriginatorSccpAddress() {
        return null;
    }

    @Override
    public String getImsiDigits() {
        return null;
    }

    @Override
    public String getNnnDigits() {
        return null;
    }

    @Override
    public ProcessingType getProcessingType() {
        return null;
    }

    @Override
    public int getErrorCode() {
        return 0;
    }

    @Override
    public long getSmppCommandStatus() {
        return 0;
    }

    @Override
    public boolean isDeliveryReceipt() {
        return false;
    }

    @Override
    public DeliveryReceiptData getDeliveryReceiptData() {
        return null;
    }

    @Override
    public Long getReceiptLocalMessageId() {
        return null;
    }

    @Override
    public MProcMessage getOriginMessageForDeliveryReceipt(final long aParamLong) {
        return null;
    }

    @Override
    public void addMprocNote(final String aNote) {
    }

    @Override
    public String getMprocNotes() {
        return null;
    }

    @Override
    public TlvSet getTlvSet() {
        return null;
    }

}
