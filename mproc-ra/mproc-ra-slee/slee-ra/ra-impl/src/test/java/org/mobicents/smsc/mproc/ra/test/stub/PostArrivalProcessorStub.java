/*
 * Telestax, Open Source Cloud Communications Copyright 2011-2017,
 * Telestax Inc and individual contributors by the @authors tag.
 * See the copyright.txt in the distribution for a full listing of
 * individual contributors.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package org.mobicents.smsc.mproc.ra.test.stub;

import java.util.Date;

import org.apache.log4j.Logger;
import org.mobicents.smsc.mproc.MProcMessage;
import org.mobicents.smsc.mproc.MProcNewMessage;
import org.mobicents.smsc.mproc.OrigType;
import org.mobicents.smsc.mproc.PostArrivalProcessor;

/**
 * The Class PostArrivalProcessorStub.
 */
public class PostArrivalProcessorStub implements PostArrivalProcessor {

    @Override
    public Logger getLogger() {
        return null;
    }

    @Override
    public void dropMessage() {
    }

    @Override
    public void rejectMessage() {
    }

    public void rejectMessage(final int anSmppErrorCode, final int aMapErrorCode, final int aHttpErrorCode) {
    }

    @Override
    public void updateMessageNetworkId(final MProcMessage aParamMProcMessage, final int aParamInt) {
    }

    @Override
    public void updateMessageDestAddrTon(final MProcMessage aParamMProcMessage, final int aParamInt) {
    }

    @Override
    public void updateMessageDestAddrNpi(final MProcMessage aParamMProcMessage, final int aParamInt) {
    }

    @Override
    public void updateMessageDestAddr(final MProcMessage aParamMProcMessage, final String aParamString) {
    }

    @Override
    public void updateMessageSourceAddrTon(final MProcMessage aParamMProcMessage, final int aParamInt) {
    }

    @Override
    public void updateMessageSourceAddrNpi(final MProcMessage aParamMProcMessage, final int aParamInt) {
    }

    @Override
    public void updateMessageSourceAddr(final MProcMessage aParamMProcMessage, final String aParamString) {
    }

    @Override
    public void updateShortMessageText(final MProcMessage aParamMProcMessage, final String aParamString) {
    }

    @Override
    public void updateShortMessageBin(final MProcMessage aParamMProcMessage, final byte[] aParamArrayOfByte) {
    }

    @Override
    public void updateScheduleDeliveryTime(final MProcMessage aParamMProcMessage, final Date aParamDate) {
    }

    @Override
    public void updateValidityPeriod(final MProcMessage aParamMProcMessage, final Date aParamDate) {
    }

    @Override
    public void updateDataCoding(final MProcMessage aParamMProcMessage, final int aParamInt) {
    }

    @Override
    public void updateDataCodingGsm7(final MProcMessage aParamMProcMessage) {
    }

    @Override
    public void updateDataCodingGsm8(final MProcMessage aParamMProcMessage) {
    }

    @Override
    public void updateDataCodingUcs2(final MProcMessage aParamMProcMessage) {
    }

    @Override
    public void updateNationalLanguageSingleShift(final MProcMessage aParamMProcMessage, final int aParamInt) {
    }

    @Override
    public void updateNationalLanguageLockingShift(final MProcMessage aParamMProcMessage, final int aParamInt) {
    }

    @Override
    public void updateEsmClass(final MProcMessage aParamMProcMessage, final int aParamInt) {
    }

    @Override
    public void updateEsmClass_ModeDatagram(final MProcMessage aParamMProcMessage) {
    }

    @Override
    public void updateEsmClass_ModeTransaction(final MProcMessage aParamMProcMessage) {
    }

    @Override
    public void updateEsmClass_ModeStoreAndForward(final MProcMessage aParamMProcMessage) {
    }

    @Override
    public void updateEsmClass_TypeNormalMessage(final MProcMessage aParamMProcMessage) {
    }

    @Override
    public void updateEsmClass_TypeDeliveryReceipt(final MProcMessage aParamMProcMessage) {
    }

    @Override
    public void updateEsmClass_UDHIndicatorPresent(final MProcMessage aParamMProcMessage) {
    }

    @Override
    public void updateEsmClass_UDHIndicatorAbsent(final MProcMessage aParamMProcMessage) {
    }

    @Override
    public void updatePriority(final MProcMessage aParamMProcMessage, final int aParamInt) {
    }

    @Override
    public void updateRegisteredDelivery(final MProcMessage aParamMProcMessage, final int aParamInt) {
    }

    @Override
    public void updateRegisteredDelivery_DeliveryReceiptNo(final MProcMessage aParamMProcMessage) {
    }

    @Override
    public void updateRegisteredDelivery_DeliveryReceiptOnSuccessOrFailure(final MProcMessage aParamMProcMessage) {
    }

    @Override
    public void updateRegisteredDelivery_DeliveryReceiptOnFailure(final MProcMessage aParamMProcMessage) {
    }

    @Override
    public void updateRegisteredDelivery_DeliveryReceiptOnSuccess(final MProcMessage aParamMProcMessage) {
    }

    @Override
    public MProcNewMessage createNewEmptyMessage(final OrigType aParamOrigType) {
        return null;
    }

    @Override
    public MProcNewMessage createNewCopyMessage(final MProcMessage aParamMProcMessage) {
        return null;
    }

    @Override
    public MProcNewMessage createNewResponseMessage(final MProcMessage aParamMProcMessage) {
        return null;
    }

    @Override
    public void postNewMessage(final MProcNewMessage aParamMProcNewMessage) {
    }

    @Override
    public void removeTlvParameter(final MProcMessage anMProcMessage, final short anArgument) {
    }

}
