/*
 * Telestax, Open Source Cloud Communications Copyright 2011-2017,
 * Telestax Inc and individual contributors by the @authors tag.
 * See the copyright.txt in the distribution for a full listing of
 * individual contributors.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package org.mobicents.smsc.mproc.ra;

/**
 * The Interface CustomMProcRuleResourceAdaptorUsageParameters.
 */
public interface CustomMProcRuleResourceAdaptorUsageParameters {
    /**
     * Increment number of total invocations counter.
     *
     * @param aValue the value
     */
    void incrementTotalInvocations(long aValue);

    /**
     * Increment MNP redirections.
     *
     * @param aValue the value
     */
    void incrementRedirections(long aValue);

    /**
     * Increment continuations.
     *
     * @param aValue the value
     */
    void incrementContinuations(long aValue);

    /**
     * Increment unsuccessful MNP redirections.
     *
     * @param aValue the value
     */
    void incrementUnsuccessfulRedirections(long aValue);

    /**
     * Increment post arrival invocations.
     *
     * @param aValue the value
     */
    void incrementPostArrivalInvocations(long aValue);

    /**
     * Increment fraud detections.
     *
     * @param aValue the value
     */
    void incrementRejections(long aValue);

    /**
     * Increment internal errors.
     *
     * @param aValue the value
     */
    void incrementInternalErrors(long aValue);

    /**
     * Increment message already rejected or dropped.
     *
     * @param aValue the value
     */
    void incrementMessageAlreadyRejectedOrDropped(long aValue);

    /**
     * Sample post arrival response time.
     *
     * @param aValue the value
     */
    void samplePostArrivalResponseTime(long aValue);

    /**
     * Increment post delivery temporary failure invocations.
     *
     * @param aValue the value
     */
    void incrementPostDeliveryTempFailureInvocations(long aValue);

    /**
     * Sample post delivery temporary failure response time.
     *
     * @param aValue the value
     */
    void samplePostDeliveryTempFailureResponseTime(long aValue);

}
