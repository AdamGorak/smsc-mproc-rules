/*
 * Telestax, Open Source Cloud Communications Copyright 2011-2017,
 * Telestax Inc and individual contributors by the @authors tag.
 * See the copyright.txt in the distribution for a full listing of
 * individual contributors.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package org.mobicents.smsc.mproc.ra;

import javax.slee.resource.ConfigProperties;
import javax.slee.resource.InvalidConfigurationException;

import org.mobicents.smsc.mproc.DeliveryReceiptData;
import org.mobicents.smsc.mproc.MProcMessage;
import org.mobicents.smsc.mproc.MProcRuleException;
import org.mobicents.smsc.mproc.MProcRuleRaException;
import org.mobicents.smsc.mproc.MProcRuleRaProvider;
import org.mobicents.smsc.mproc.PostArrivalProcessor;
import org.mobicents.smsc.mproc.PostDeliveryProcessor;
import org.mobicents.smsc.mproc.PostDeliveryTempFailureProcessor;
import org.mobicents.smsc.mproc.PostHrSriProcessor;
import org.mobicents.smsc.mproc.PostImsiProcessor;
import org.mobicents.smsc.mproc.PostPreDeliveryProcessor;
import org.mobicents.smsc.mproc.abl.CustomMProcRuleLogic;
import org.mobicents.smsc.mproc.abl.CustomMProcRuleLogicRequest;
import org.mobicents.smsc.mproc.abl.CustomMProcRuleLogicResult;

import com.unifonic.smsc.abl.UnifonicMprocRaAblRequest;

/**
 * The Class CustomMProcRuleResourceAdaptor.
 */
public final class CustomMProcRuleResourceAdaptor extends CustomMProcRuleResourceAdaptorBase
        implements MProcRuleRaProvider {

    private static final String P_NODE_NAME = "Node.Name";
    private static final String P_DB_DATA_SOURCE_JNDI = "Db.DataSource.Jndi";
    private static final String P_DB_URL = "Db.Url";
    private static final String P_DB_USER = "Db.User";
    private static final String P_DB_PASS = "Db.Password";
    private static final String P_DB_DRIVER = "Db.DriverClassName";
    private static final String P_DB_VALIDATION_QUERY = "Db.ValidationQuery";
    private static final String P_DB_CONNECTION_PROPERTIES = "Db.ConnectionProperties";
    private static final String P_CONTEXT_FILE_NAME = "Context.File.Name";

    private static final String[] PARAMETERS = { P_NODE_NAME, P_DB_DATA_SOURCE_JNDI, P_DB_URL, P_DB_USER, P_DB_PASS,
            P_DB_DRIVER, P_DB_VALIDATION_QUERY, P_DB_CONNECTION_PROPERTIES, P_CONTEXT_FILE_NAME };

    private static final long ONE = 1l;

    private String itsContextFileName;

    private CustomMProcRuleLogic itsLogic;

    private CustomMProcRuleResourceAdaptorUsageParameters itsUsage;

    /**
     * Instantiates a new MProc rule resource adaptor.
     */
    public CustomMProcRuleResourceAdaptor() {
        super();
    }

    @Override
    public Object getResourceAdaptorInterface(final String aName) {
        return this;
    }

    @Override
    public void onPostArrival(final PostArrivalProcessor aProcessor, final MProcMessage aMessage)
            throws MProcRuleRaException {
        if (isNotActive()) {
            warn("RA is not active.");
            return;
        }
        final long start = System.currentTimeMillis();
        handlePostArrival(aProcessor, aMessage, start);
        itsUsage.samplePostArrivalResponseTime(System.currentTimeMillis() - start);
    }

    @Override
    public void onPostDeliveryTempFailure(final PostDeliveryTempFailureProcessor aProcessor,
            final MProcMessage aMessage) {
        if (isNotActive()) {
            warn("RA is not active.");
            return;
        }
        final long start = System.currentTimeMillis();
        handlePostDeliveryTempFailure(aProcessor, aMessage, start);
        itsUsage.samplePostDeliveryTempFailureResponseTime(System.currentTimeMillis() - start);
    }

    @Override
    public void onPostDelivery(final PostDeliveryProcessor aProcessor, final MProcMessage aMessage) {
        if (isNotActive()) {
            warn("RA is not active.");
            return;
        }
        itsUsage.incrementTotalInvocations(ONE);
        fine(convert(aMessage), " - Post Delivery.");
    }

    @Override
    public void onPostPreDelivery(final PostPreDeliveryProcessor aProcessor, final MProcMessage aMessage) {
        if (isNotActive()) {
            warn("RA is not active.");
            return;
        }
        itsUsage.incrementTotalInvocations(ONE);
        fine(convert(aMessage), " - Post Pre Delivery.");
    }

    @Override
    public void onPostHrSri(final PostHrSriProcessor aProcessor, final MProcMessage aMessage) {
        if (isNotActive()) {
            warn("RA is not active.");
            return;
        }
        itsUsage.incrementTotalInvocations(ONE);
        fine(convert(aMessage), " - Post HR SRI.");
    }

    @Override
    public void onPostImsiRequest(final PostImsiProcessor aProcessor, final MProcMessage aMessage) {
        if (isNotActive()) {
            warn("RA is not active.");
            return;
        }
        itsUsage.incrementTotalInvocations(ONE);
        fine(convert(aMessage), " - Post IMSI.");
    }

    @Override
    public void invokeTaskAsynch(final String aTask) {
        if (isNotActive()) {
            warn("RA is not active.");
            return;
        }
        itsUsage.incrementTotalInvocations(ONE);
    }

    @Override
    public void invokeTaskAsynch(final Object aTask) {
        if (isNotActive()) {
            warn("RA is not active.");
            return;
        }
        itsUsage.incrementTotalInvocations(ONE);
    }

    @Override
    public String invokeTaskSynch(final String aTask) {
        if (isNotActive()) {
            warn("RA is not active.");
            return null;
        }
        itsUsage.incrementTotalInvocations(ONE);
        return null;
    }

    @Override
    public Object invokeTaskSynch(final Object aTask) {
        if (isNotActive()) {
            warn("RA is not active.");
            return null;
        }
        itsUsage.incrementTotalInvocations(ONE);
        return null;
    }

    @Override
    protected void handleRaVerifyConfiguration(final ConfigProperties aProperties)
            throws InvalidConfigurationException {
        itsContextFileName = validateNotEmpty(aProperties, P_CONTEXT_FILE_NAME);
    }

    @Override
    protected void handleRaActive() {
        itsLogic = getBean(CustomMProcRuleLogic.class);
        itsUsage = (CustomMProcRuleResourceAdaptorUsageParameters) getDefaultUsageParameterSet();
    }

    @Override
    protected Class<?> getRaClass() {
        return CustomMProcRuleResourceAdaptor.class;
    }

    @Override
    protected String getContextFileName() {
        return itsContextFileName;
    }

    @Override
    protected String[] getRaConfiguration() {
        return PARAMETERS;
    }

    private void handlePostArrival(final PostArrivalProcessor aProcessor, final MProcMessage aMessage,
            final long aStart) throws MProcRuleRaException {
        itsUsage.incrementTotalInvocations(ONE);
        itsUsage.incrementPostArrivalInvocations(ONE);
        final CustomMProcRuleLogicRequest req = convert(aMessage);
        final CustomMProcRuleLogicResult res = itsLogic.handlePostArrival(req);
        if (res == null) {
            itsUsage.incrementInternalErrors(ONE);
            throw new MProcRuleRaException("Unsuccessful ABL execution (null result).");
        }
        if (res.hasCallDetailRecord()) {
            aMessage.addMprocNote(res.getCallDetailRecord());
        }
        setSenderId(aProcessor, aMessage, req, res);
        updateSourceAddressTon(aProcessor, aMessage, req, res);
        updateDestinationAddressTon(aProcessor, aMessage, req, res);
        if (res.isContinue()) {
            fine(req, " -> CUE.");
            itsUsage.incrementContinuations(ONE);
            return;
        }
        if (res.isRedirect()) {
            fine(req, " -> RER(", res.getNewNetworkId(), ").");
            itsUsage.incrementRedirections(ONE);
            final Integer nnwid = res.getNewNetworkId();
            if (nnwid == null) {
                warn(req, "Redirect but New NetworkId is not set?");
            } else {
                aProcessor.updateMessageNetworkId(aMessage, nnwid.intValue());
            }
            return;
        }
        if (res.isReject()) {
            fine(req, " -> REJ(", res.getRejectSmppError(), ", ", res.getRejectHttpError(), ", ",
                    res.getRejectMapError(), ").");
            itsUsage.incrementRejections(ONE);
            try {
                aProcessor.rejectMessage(res.getRejectSmppError(), res.getRejectMapError(), res.getRejectHttpError());
            } catch (MProcRuleException e) {
                if (e.isActionAlreadyAdded()) {
                    itsUsage.incrementMessageAlreadyRejectedOrDropped(ONE);
                    info("Unable to reject: ", req, ". Response: ", res, ". Message already rejected or dropped.");
                } else {
                    itsUsage.incrementInternalErrors(ONE);
                    warn(e, "Unable to reject: ", req, ". Response: ", res, ".");
                    throw new MProcRuleRaException(e);
                }
            }
        }
    }

    private void setSenderId(final PostArrivalProcessor aProcessor, final MProcMessage aMessage,
            final CustomMProcRuleLogicRequest aRequest, final CustomMProcRuleLogicResult aResult)
            throws MProcRuleRaException {
        final String sid = aResult.getSenderId();
        if (sid == null) {
            return;
        }
        if (sid.isEmpty()) {
            return;
        }
        fine(aRequest, " -> SenderID(", sid, ").");
        try {
            aProcessor.updateMessageSourceAddr(aMessage, sid);
        } catch (MProcRuleException e) {
            itsUsage.incrementInternalErrors(ONE);
            warn(e, "Unable to update sender for: ", aRequest, ". Response: ", aResult, ".");
            throw new MProcRuleRaException(e);
        }
    }

    private void updateSourceAddressTon(final PostArrivalProcessor aProcessor, final MProcMessage aMessage,
            final CustomMProcRuleLogicRequest aRequest, final CustomMProcRuleLogicResult aResult)
            throws MProcRuleRaException {
        final Integer ston = aResult.getNewSourceTon();
        if (isSet(ston)) {
            try {
                aProcessor.updateMessageSourceAddrTon(aMessage, ston);
            } catch (MProcRuleException e) {
                if (e.isActionAlreadyAdded()) {
                    itsUsage.incrementMessageAlreadyRejectedOrDropped(ONE);
                    info("Unable to update source TON on: ", aRequest, ". Response: ", aResult,
                            ". Message already rejected or dropped.");
                } else {
                    itsUsage.incrementInternalErrors(ONE);
                    warn(e, "Unable to update source TOn on: ", aRequest, ". Response: ", aResult, ".");
                    throw new MProcRuleRaException(e);
                }
            }
        }
    }

    private void updateDestinationAddressTon(final PostArrivalProcessor aProcessor, final MProcMessage aMessage,
            final CustomMProcRuleLogicRequest aRequest, final CustomMProcRuleLogicResult aResult)
            throws MProcRuleRaException {
        final Integer dton = aResult.getNewDestinationTon();
        if (isSet(dton)) {
            try {
                aProcessor.updateMessageDestAddrTon(aMessage, dton);
            } catch (MProcRuleException e) {
                if (e.isActionAlreadyAdded()) {
                    itsUsage.incrementMessageAlreadyRejectedOrDropped(ONE);
                    info("Unable to update destination TON on: ", aRequest, ". Response: ", aResult,
                            ". Message already rejected or dropped.");
                } else {
                    itsUsage.incrementInternalErrors(ONE);
                    warn(e, "Unable to update destination TOn on: ", aRequest, ". Response: ", aResult, ".");
                    throw new MProcRuleRaException(e);
                }
            }
        }
    }

    private void handlePostDeliveryTempFailure(final PostDeliveryTempFailureProcessor aProcessor,
            final MProcMessage aMessage, final long aStart) {
        itsUsage.incrementTotalInvocations(ONE);
        itsUsage.incrementPostDeliveryTempFailureInvocations(ONE);
        itsLogic.handlePostDeliveryTempFailure(convert(aMessage));
    }

    private static CustomMProcRuleLogicRequest convert(final MProcMessage aMessage) {
        return new UnifonicMprocRaAblRequest(aMessage.getOrigNetworkId(), aMessage.getNetworkId(),
                aMessage.getShortMessageText(), aMessage.getSourceAddr(), aMessage.getDestAddr(),
                aMessage.isDeliveryReceipt(), getReceiptLocalMessageId(aMessage),
                getDeliveryReceiptDataError(aMessage));
    }

    private static String getReceiptLocalMessageId(final MProcMessage aMessage) {
        final Long rlmid = aMessage.getReceiptLocalMessageId();
        if (rlmid == null) {
            return null;
        }
        return String.valueOf(rlmid);
    }

    private static int getDeliveryReceiptDataError(final MProcMessage aMessage) {
        final DeliveryReceiptData drd = aMessage.getDeliveryReceiptData();
        if (drd == null) {
            return 0;
        }
        return drd.getError();
    }

    private static boolean isSet(final Integer aValue) {
        if (aValue == null) {
            return false;
        }
        if (aValue < 0) {
            return false;
        }
        return true;
    }
}
