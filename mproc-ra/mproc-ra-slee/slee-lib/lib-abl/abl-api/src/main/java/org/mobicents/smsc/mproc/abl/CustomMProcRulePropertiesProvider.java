/*
 * COPYRIGHT Unifonic Inc. 2017.
 * The copyright to the computer program(s) herein is the property of Unifonic.
 * The programs may be used and/or copied only with written permission from
 * Unifonic.
 */
package org.mobicents.smsc.mproc.abl;

/**
 * The Interface CustomMProcRulePropertiesProvider.
 */
public interface CustomMProcRulePropertiesProvider {

    /**
     * Gets the property.
     *
     * @param aName the name
     * @return the property
     */
    String getProperty(final String aName);

}
