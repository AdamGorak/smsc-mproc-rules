/*
 * Telestax, Open Source Cloud Communications Copyright 2011-2017,
 * Telestax Inc and individual contributors by the @authors tag.
 * See the copyright.txt in the distribution for a full listing of
 * individual contributors.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package org.mobicents.smsc.mproc.abl;

/**
 * The Interface UnifonicMprocRaAblResult.
 */
public interface CustomMProcRuleLogicResult {
    
    /**
     * Checks if is continue.
     *
     * @return true, if is continue
     */
    boolean isContinue();

    /**
     * Checks if is redirect.
     *
     * @return true, if is redirect
     */
    boolean isRedirect();

    /**
     * Checks if is reject.
     *
     * @return true, if is reject
     */
    boolean isReject();

    /**
     * Gets the reject SMPP error.
     *
     * @return the reject SMPP error
     */
    int getRejectSmppError();

    /**
     * Gets the reject HTTP error.
     *
     * @return the reject HTTP error
     */
    int getRejectHttpError();

    /**
     * Gets the reject MAP error.
     *
     * @return the reject MAP error
     */
    int getRejectMapError();

    /**
     * Gets the new network ID.
     *
     * @return the new network ID
     */
    Integer getNewNetworkId();

    /**
     * Gets the new source TON.
     *
     * @return the new source TON
     */
    Integer getNewSourceTon();

    /**
     * Gets the new destination TON.
     *
     * @return the new destination TON
     */
    Integer getNewDestinationTon();

    /**
     * Gets the sender id.
     *
     * @return the sender id
     */
    String getSenderId();

    /**
     * Checks for call detail record.
     *
     * @return true, if successful
     */
    boolean hasCallDetailRecord();

    /**
     * Gets the call detail record.
     *
     * @return the call detail record
     */
    String getCallDetailRecord();
    
}