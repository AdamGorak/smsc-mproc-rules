/*
 * Telestax, Open Source Cloud Communications Copyright 2011-2017,
 * Telestax Inc and individual contributors by the @authors tag.
 * See the copyright.txt in the distribution for a full listing of
 * individual contributors.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package org.mobicents.smsc.mproc.abl;

/**
 * The Interface CustomMProcRuleLogicRequest.
 */
public interface CustomMProcRuleLogicRequest {

    /**
     * Gets the origin network ID.
     *
     * @return the origin network ID
     */
    int getOriginNetworkId();

    /**
     * Gets the network ID.
     *
     * @return the network ID
     */
    int getNetworkId();

    /**
     * Gets the short message text 20.
     *
     * @return the short message text 20
     */
    String getShortMessageText20();

    /**
     * Gets the short message text.
     *
     * @return the short message text
     */
    String getShortMessageText();

    /**
     * Gets the source address.
     *
     * @return the source address
     */
    String getSourceAddress();

    /**
     * Gets the destination address.
     *
     * @return the destination address
     */
    String getDestinationAddress();
    
    /**
     * Gets the receipt local message ID.
     *
     * @return the receipt local message ID
     */
    long getReceiptLocalMessageId();

    /**
     * Checks if is delivery receipt.
     *
     * @return true, if is delivery receipt
     */
    boolean isDeliveryReceipt();

    /**
     * Gets the delivery receipt data error.
     *
     * @return the delivery receipt data error
     */
    Integer getDeliveryReceiptDataError();
    
    /**
     * Gets the error code.
     *
     * @return the error code
     */
    int getErrorCode();

}