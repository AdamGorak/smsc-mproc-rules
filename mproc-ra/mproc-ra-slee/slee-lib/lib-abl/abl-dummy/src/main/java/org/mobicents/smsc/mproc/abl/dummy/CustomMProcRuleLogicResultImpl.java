/*
 * Telestax, Open Source Cloud Communications Copyright 2011-2017,
 * Telestax Inc and individual contributors by the @authors tag.
 * See the copyright.txt in the distribution for a full listing of
 * individual contributors.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package org.mobicents.smsc.mproc.abl.dummy;

import org.mobicents.smsc.mproc.abl.CustomMProcRuleLogicResult;

/**
 * The Class CustomMProcRuleLogicResultImpl.
 */
public final class CustomMProcRuleLogicResultImpl implements CustomMProcRuleLogicResult {

    private final boolean itsContinue;

    private final boolean itsReject;
    private final int itsRejectSmppError;
    private final int itsRejectHttpError;
    private final int itsRejectMapError;

    private final boolean itsRedirect;
    private final int itsNewNetworkId;
    private final String itsSenderId;

    private final String itsCallDetailRecord;

    /**
     * Instantiates a new unifonic MProc RA ABL result.
     *
     * @param aCallDetailRecord the call detail record
     */
    public CustomMProcRuleLogicResultImpl(final String aCallDetailRecord) {
        itsContinue = true;
        itsRedirect = false;
        itsReject = false;
        itsNewNetworkId = 0;
        itsSenderId = null;
        itsRejectSmppError = 0;
        itsRejectHttpError = 0;
        itsRejectMapError = 0;
        itsCallDetailRecord = aCallDetailRecord;
    }

    /**
     * Instantiates a new unifonic MProc RA ABL result.
     *
     * @param aNewNetworkId the new network ID
     * @param aCallDetailRecord the call detail record
     */
    public CustomMProcRuleLogicResultImpl(final int aNewNetworkId, final String aSenderId,
            final String aCallDetailRecord) {
        itsContinue = false;
        itsRedirect = true;
        itsReject = false;
        itsNewNetworkId = aNewNetworkId;
        itsSenderId = aSenderId;
        itsRejectSmppError = 0;
        itsRejectHttpError = 0;
        itsRejectMapError = 0;
        itsCallDetailRecord = aCallDetailRecord;
    }

    /**
     * Instantiates a new unifonic MProc RA ABL result.
     *
     * @param aRejectSmppError the reject SMPP error
     * @param aRejectHttpError the reject HTTP error
     * @param aRejectMapError the reject MAP error
     * @param aCallDetailRecord the call detail record
     */
    public CustomMProcRuleLogicResultImpl(final int aRejectSmppError, final int aRejectHttpError,
            final int aRejectMapError, final String aCallDetailRecord) {
        itsContinue = false;
        itsRedirect = false;
        itsReject = true;
        itsNewNetworkId = 0;
        itsSenderId = null;
        itsRejectSmppError = aRejectSmppError;
        itsRejectHttpError = aRejectHttpError;
        itsRejectMapError = aRejectMapError;
        itsCallDetailRecord = aCallDetailRecord;
    }

    @Override
    public boolean isReject() {
        return itsReject;
    }

    @Override
    public int getRejectSmppError() {
        return itsRejectSmppError;
    }

    @Override
    public int getRejectHttpError() {
        return itsRejectHttpError;
    }

    @Override
    public int getRejectMapError() {
        return itsRejectMapError;
    }

    @Override
    public boolean isRedirect() {
        return itsRedirect;
    }

    @Override
    public Integer getNewNetworkId() {
        return itsNewNetworkId;
    }

    @Override
    public String getSenderId() {
        return itsSenderId;
    }

    @Override
    public boolean isContinue() {
        return itsContinue;
    }

    @Override
    public String getCallDetailRecord() {
        return itsCallDetailRecord;
    }

    @Override
    public boolean hasCallDetailRecord() {
        return itsCallDetailRecord != null;
    }

    @Override
    public Integer getNewSourceTon() {
        return 0;
    }

    @Override
    public Integer getNewDestinationTon() {
        return 0;
    }

}
