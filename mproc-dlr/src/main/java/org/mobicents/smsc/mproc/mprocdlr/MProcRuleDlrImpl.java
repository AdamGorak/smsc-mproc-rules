package org.mobicents.smsc.mproc.mprocdlr;

import javolution.xml.XMLFormat;
import javolution.xml.stream.XMLStreamException;
import org.apache.log4j.Logger;
import org.mobicents.smsc.mproc.*;

import java.util.*;

/**
 * The Class MProcRuleDlrImpl.
 */
public class MProcRuleDlrImpl extends MProcRuleBaseImpl {

    private static final Logger LOG = Logger.getLogger(MProcRuleDlrImpl.class);

    private static final String PARAMETER_NETWORK_ID = "networkidmask";
    private static final String PARAMETER_NETWORK_ID_XML = "networkIdMask";

    private static final String PARAMETER_NEW_REGISTERED_DELIVERY = "newregistereddelivery";
    private static final String PARAMETER_NEW_REGISTERED_DELIVERY_XML = "newRegisteredDelivery";

    private static final String PARAMETER_NEW_REGISTERED_POST_DELIVERY = "newregisteredpostdelivery";
    private static final String PARAMETER_NEW_REGISTERED_POST_DELIVERY_XML = "newRegisteredPostDelivery";

    private static final String CACHE_SIZE = "cachesize";
    private static final String POST_ARRIVAL = "PostArrival";
    private static final String POST_PRE_DELIVERY = "PostPreDelivery";

    private static final String DELIVERY_ACK_STATE_DELIVERED = "DELIVRD";
    private static final String DELIVERY_ACK_STATE_UNDELIVERABLE = "UNDELIV";
    private static final String DELIVERY_ACK_STATE_ENROUTE = "ENROUTE";
    private static final String SET_RULE_PARAMETERS_FAIL_NO_ACTIONS_PROVIDED = "Setting of MProcRule parameters failed: no action is provided";

    private int itsRegisteredDeliveryValue = -1;
    private int itsRegisteredPostDeliveryValue = -1;
    private String itsNetworkIdsListString = "";

    private List<Integer> networkIdsList = new ArrayList<Integer>();
    private Map<String, Integer> cacheMap = new HashMap<String, Integer>();

    /**
     * Instantiates a new MProc rule dlr implementation.
     */
    public MProcRuleDlrImpl() {
        super();
    }

    @Override
    public void setInitialRuleParameters(final String aParametersString) throws MProcRuleDlrException {
        setRuleParameters(aParametersString);
    }

    @Override
    public void updateRuleParameters(final String aParametersString) throws MProcRuleDlrException {
        setRuleParameters(aParametersString);
    }

    @Override
    public String getRuleClassName() {
        return MProcRuleDlrFactoryImpl.CLASS_NAME;
    }

    @Override
    public String getRuleParameters() {
        StringBuilder sb = new StringBuilder();
        int parNumber = 0;
        if (!"".equals(itsNetworkIdsListString)) {
            writeParameter(sb, parNumber++, PARAMETER_NETWORK_ID, itsNetworkIdsListString, " ", " ");
        }
        if (itsRegisteredDeliveryValue != -1) {
            writeParameter(sb, parNumber++, PARAMETER_NEW_REGISTERED_DELIVERY, itsRegisteredDeliveryValue, " ", " ");
        }
        if (itsRegisteredPostDeliveryValue != -1) {
            writeParameter(sb, parNumber++, PARAMETER_NEW_REGISTERED_POST_DELIVERY, itsRegisteredPostDeliveryValue, " ", " ");
        }
        writeParameter(sb, parNumber++, CACHE_SIZE, cacheMap.size(), " (", " ");
        sb.append(")");
        return sb.toString();
    }

    @Override
    public boolean isForPostArrivalState() {
        return true;
    }

    @Override
    public boolean isForPostPreDeliveryState() {
        return true;
    }

    @Override
    public boolean matchesPostArrival(final MProcMessage aMessage) {
        return matches(aMessage, POST_ARRIVAL);
    }

    @Override
    public boolean matchesPostPreDelivery(MProcMessage messageDest) {
        return matches(messageDest, POST_PRE_DELIVERY);
    }

    @Override
    public void onPostArrival(final MProcRuleRaProvider anMProcRuleRa, final PostArrivalProcessor aProcessor,
                              final MProcMessage aMessage) throws MProcRuleException {
        if (!aMessage.isDeliveryReceipt()) {
            handleRegisteredDelivery(aProcessor, aMessage);
        }
    }

    @Override
    public void onPostPreDelivery(MProcRuleRaProvider anMProcRuleRa, PostPreDeliveryProcessor aProcessor, MProcMessage aMessage) throws Exception {
        if (aMessage.isDeliveryReceipt()) {
            handleDeliveryReceipt(aProcessor, aMessage);
        }
    }

    private void handleRegisteredDelivery(final PostArrivalProcessor aProcessor, final MProcMessage aMessage) {
        final int rd = aMessage.getRegisteredDelivery();
        if (itsRegisteredDeliveryValue == -1 || rd == itsRegisteredDeliveryValue) {
            return;
        }
//        long mid = Long.parseLong(aMessage.getMessageId());
        long mid = aMessage.getMessageId();
        aProcessor.updateRegisteredDelivery(aMessage, itsRegisteredDeliveryValue);
        if (itsRegisteredDeliveryValue != 0) {
            cacheMap.put(String.valueOf(mid), rd);
            if (LOG.isDebugEnabled()) {
                LOG.debug("MProcRuleDlr: messageId=" + mid + " inserted to cache");
            }
        }
        LOG.info("MProcRuleDlr: Registered delivery value changed: " + rd + " => " + itsRegisteredDeliveryValue + " for messageId=" + mid);
    }

    private void handleDeliveryReceipt(final PostPreDeliveryProcessor aProcessor, final MProcMessage aMessage) throws MProcRuleException {
        Long receiptLocalMessageId = aMessage.getReceiptLocalMessageId();
        DeliveryReceiptData deliveryReceiptData = aMessage.getDeliveryReceiptData();

        if (receiptLocalMessageId != null && deliveryReceiptData != null) {
            Integer originalDeliveryReceipt = cacheMap.get(String.valueOf(receiptLocalMessageId));
            if (LOG.isDebugEnabled()) {
                LOG.debug("MProcRuleDlr: no message in cache for messageId=" + receiptLocalMessageId);
            }
            if (originalDeliveryReceipt == null) {
                if (itsRegisteredPostDeliveryValue == -1) {
                    return;
                } else {
                    originalDeliveryReceipt = itsRegisteredPostDeliveryValue;
                }
            } else {
                if (itsRegisteredPostDeliveryValue != -1 && originalDeliveryReceipt != itsRegisteredPostDeliveryValue) {
                    originalDeliveryReceipt = itsRegisteredPostDeliveryValue;
                }
                cacheMap.remove(String.valueOf(receiptLocalMessageId));
                if (LOG.isDebugEnabled()) {
                    LOG.debug("MProcRuleDlr: message messageId=" + receiptLocalMessageId + " removed from cache");
                }
            }

            Integer tlvMessageState = deliveryReceiptData.getTlvMessageState();
            String messageStatus = deliveryReceiptData.getStatus();

            if (tlvMessageState == null && (messageStatus == null || "".equals(messageStatus))) {
                return;
            }
            if (tlvMessageState != null && messageStatus != null && !"".equals(messageStatus)) {
                if ((messageStatus.equals(DELIVERY_ACK_STATE_DELIVERED) && tlvMessageState != 2)
                        || (messageStatus.equals(DELIVERY_ACK_STATE_UNDELIVERABLE) && tlvMessageState != 5)
                        || (messageStatus.equals(DELIVERY_ACK_STATE_ENROUTE) && tlvMessageState == 2 || tlvMessageState == 5)) {
                    return;
                }
            }
            if (itsRegisteredDeliveryValue == 0) {
                aProcessor.dropMessage();
                return;
            } else if (itsRegisteredDeliveryValue == 2) {
                if (DELIVERY_ACK_STATE_DELIVERED.equals(messageStatus)) {
                    aProcessor.dropMessage();
                    return;
                }
            } else if (itsRegisteredDeliveryValue == 3) {
                if (!DELIVERY_ACK_STATE_DELIVERED.equals(messageStatus)) {
                    aProcessor.dropMessage();
                    return;
                }
            }
            checkDlrWithOriginalReceipt(tlvMessageState, originalDeliveryReceipt, aProcessor, messageStatus);
        }
    }

    private void checkDlrWithOriginalReceipt(final Integer tlvMessageState, final Integer originalDeliveryReceipt,
                                          final PostPreDeliveryProcessor aProcessor, final String messageStatus) throws MProcRuleException {
        if (tlvMessageState != null) {
            switch (originalDeliveryReceipt) {
                case 0:
                    aProcessor.dropMessage();
                    break;
                case 1:
                    switch (tlvMessageState) {
                        case 0:
                        case 1:
                        case 6:
                        case 7:
                            aProcessor.dropMessage();
                            break;
                    }
                    break;
                case 2:
                    switch (tlvMessageState) {
                        case 0:
                        case 1:
                        case 2:
                        case 6:
                        case 7:
                            aProcessor.dropMessage();
                            break;
                    }
                    break;
                case 3:
                    if (tlvMessageState != 2)
                        aProcessor.dropMessage();
                    break;
            }
        } else {
            switch (originalDeliveryReceipt) {
                case 0:
                    aProcessor.dropMessage();
                    break;
                case 1:
                    if (messageStatus.equals(DELIVERY_ACK_STATE_ENROUTE))
                        aProcessor.dropMessage();
                    break;
                case 2:
                    if (!messageStatus.equals(DELIVERY_ACK_STATE_UNDELIVERABLE))
                        aProcessor.dropMessage();
                    break;
                case 3:
                    if (!messageStatus.equals(DELIVERY_ACK_STATE_DELIVERED))
                        aProcessor.dropMessage();
                    break;
            }
        }
    }

    private void setRuleParameters(final String parametersString) throws MProcRuleDlrException {
        String[] args = splitParametersString(parametersString);
        networkIdsList.clear();
        int count = 0;
        String command;
        boolean success = false;

        while (count < args.length) {
            command = args[count++];
            if (count < args.length) {
                String value = args[count++];
                if (command.equals(PARAMETER_NETWORK_ID)) {
                    itsNetworkIdsListString = value;
                    String[] splitted = value.split(",");
                    for (String id : splitted) {
                        try {
                            networkIdsList.add(Integer.parseInt(id));
                        } catch (NumberFormatException e) {
                            throw new MProcRuleDlrException("Invalid rule parameter value: " + id + ".");
                        }
                    }
                } else if (command.equals(PARAMETER_NEW_REGISTERED_DELIVERY)) {
                    try {
                        this.itsRegisteredDeliveryValue = Integer.parseInt(value);
                        success = true;
                    } catch (NumberFormatException e) {
                        throw new MProcRuleDlrException("Invalid rule parameter value: " + value + ".");
                    }
                } else if (command.equals(PARAMETER_NEW_REGISTERED_POST_DELIVERY)) {
                    try {
                        this.itsRegisteredPostDeliveryValue = Integer.parseInt(value);
                    } catch (NumberFormatException e) {
                        throw new MProcRuleDlrException("Invalid rule parameter value: " + value + ".");
                    }
                }
            }
        }
        if (!success) {
            throw new MProcRuleDlrException(SET_RULE_PARAMETERS_FAIL_NO_ACTIONS_PROVIDED);
        }
    }

    private boolean matches(final MProcMessage aMessage, final String aMethod) {

        if (networkIdsList.isEmpty()) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("Rule: #" + getId() + ". " + aMethod + ". NetworkId is not set - accepting.");
            }
            return true;
        }
        if (LOG.isDebugEnabled()) {
            LOG.debug("Rule: #" + getId() + ". " + aMethod + ". NetworkIds: " + networkIdsList + ". Message NetworkId: "
                    + aMessage.getNetworkId() + ".");
        }
        for (int id : networkIdsList) {
            if (id == aMessage.getNetworkId()) {
                return true;
            }
        }
        return false;
    }

    protected static final XMLFormat<MProcRuleDlrImpl> M_PROC_RULE_RA_IMPL_XML = new XMLFormat<MProcRuleDlrImpl>(
            MProcRuleDlrImpl.class) {

        public void read(final InputElement anXml, final MProcRuleDlrImpl aRule) throws XMLStreamException {
            M_PROC_RULE_BASE_XML.read(anXml, aRule);
            aRule.itsNetworkIdsListString = anXml.getAttribute(PARAMETER_NETWORK_ID_XML, "-1");
            String[] splitted = aRule.itsNetworkIdsListString.split(",");
            for (String id : splitted) {
                aRule.networkIdsList.add(Integer.parseInt(id));
            }
            aRule.itsRegisteredDeliveryValue = anXml.getAttribute(PARAMETER_NEW_REGISTERED_DELIVERY_XML, -1);
            aRule.itsRegisteredPostDeliveryValue = anXml.getAttribute(PARAMETER_NEW_REGISTERED_POST_DELIVERY_XML, -1);
        }

        public void write(final MProcRuleDlrImpl aRule, final OutputElement anXml) throws XMLStreamException {
            M_PROC_RULE_BASE_XML.write(aRule, anXml);
            if (!"".equals((aRule.itsNetworkIdsListString))) {
                anXml.setAttribute(PARAMETER_NETWORK_ID_XML, aRule.itsNetworkIdsListString);
            }
            if (aRule.itsRegisteredDeliveryValue != -1)
                anXml.setAttribute(PARAMETER_NEW_REGISTERED_DELIVERY_XML, aRule.itsRegisteredDeliveryValue);
            if (aRule.itsRegisteredPostDeliveryValue != -1)
                anXml.setAttribute(PARAMETER_NEW_REGISTERED_POST_DELIVERY_XML, aRule.itsRegisteredPostDeliveryValue);
        }
    };
}
