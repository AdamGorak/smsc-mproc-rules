package org.mobicents.smsc.mproc.mprocdlr;

import org.mobicents.smsc.mproc.MProcRule;
import org.mobicents.smsc.mproc.MProcRuleFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * The Class MProcRuleDlrFactoryImpl.
 */
public class MProcRuleDlrFactoryImpl implements MProcRuleFactory {

    public static final String CLASS_NAME = "mprocdlr";

    public String getRuleClassName() {
        return CLASS_NAME;
    }

    public MProcRule createMProcRuleInstance() {
        return new MProcRuleDlrImpl();
    }
}
